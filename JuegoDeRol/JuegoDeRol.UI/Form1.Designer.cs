﻿
namespace JuegoDeRol.UI
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.P1_groupBox = new System.Windows.Forms.GroupBox();
            this.P1_comboBoxCurar = new System.Windows.Forms.ComboBox();
            this.P1_listaPD = new System.Windows.Forms.ListBox();
            this.P1_listaArmas = new System.Windows.Forms.ListBox();
            this.P1_botonCurar = new System.Windows.Forms.Button();
            this.P1_listaAD = new System.Windows.Forms.ListBox();
            this.P1_botonAtacar = new System.Windows.Forms.Button();
            this.P1_listaEjercito = new System.Windows.Forms.ListBox();
            this.P2_labelAD = new System.Windows.Forms.Label();
            this.P2_labelSalud = new System.Windows.Forms.Label();
            this.P1_labelPD = new System.Windows.Forms.Label();
            this.P1_labelArma = new System.Windows.Forms.Label();
            this.P1_labelEjercito = new System.Windows.Forms.Label();
            this.P1_labelAD = new System.Windows.Forms.Label();
            this.P1_labelSalud = new System.Windows.Forms.Label();
            this.P2_groupBox = new System.Windows.Forms.GroupBox();
            this.P2_comboBoxCurar = new System.Windows.Forms.ComboBox();
            this.P2_listaPD = new System.Windows.Forms.ListBox();
            this.P2_listaArmas = new System.Windows.Forms.ListBox();
            this.P2_botonCurar = new System.Windows.Forms.Button();
            this.P2_listaAD = new System.Windows.Forms.ListBox();
            this.P2_botonAtacar = new System.Windows.Forms.Button();
            this.P2_listaEjercito = new System.Windows.Forms.ListBox();
            this.P2_labelEjercito = new System.Windows.Forms.Label();
            this.P2_labelPD = new System.Windows.Forms.Label();
            this.P2_labelArma = new System.Windows.Forms.Label();
            this.boton_AgarrarArma = new System.Windows.Forms.Button();
            this.imagenCofre = new System.Windows.Forms.PictureBox();
            this.textBoxCajon = new System.Windows.Forms.TextBox();
            this.P1_groupBox.SuspendLayout();
            this.P2_groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imagenCofre)).BeginInit();
            this.SuspendLayout();
            // 
            // P1_groupBox
            // 
            this.P1_groupBox.BackColor = System.Drawing.Color.Transparent;
            this.P1_groupBox.Controls.Add(this.P1_comboBoxCurar);
            this.P1_groupBox.Controls.Add(this.P1_listaPD);
            this.P1_groupBox.Controls.Add(this.P1_listaArmas);
            this.P1_groupBox.Controls.Add(this.P1_botonCurar);
            this.P1_groupBox.Controls.Add(this.P1_listaAD);
            this.P1_groupBox.Controls.Add(this.P1_botonAtacar);
            this.P1_groupBox.Controls.Add(this.P1_listaEjercito);
            this.P1_groupBox.Controls.Add(this.P2_labelAD);
            this.P1_groupBox.Controls.Add(this.P2_labelSalud);
            this.P1_groupBox.Controls.Add(this.P1_labelPD);
            this.P1_groupBox.Controls.Add(this.P1_labelArma);
            this.P1_groupBox.Controls.Add(this.P1_labelEjercito);
            this.P1_groupBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.P1_groupBox.Location = new System.Drawing.Point(12, 12);
            this.P1_groupBox.Name = "P1_groupBox";
            this.P1_groupBox.Size = new System.Drawing.Size(515, 239);
            this.P1_groupBox.TabIndex = 1;
            this.P1_groupBox.TabStop = false;
            this.P1_groupBox.Text = "JUGADOR 1";
            // 
            // P1_comboBoxCurar
            // 
            this.P1_comboBoxCurar.Enabled = false;
            this.P1_comboBoxCurar.FormattingEnabled = true;
            this.P1_comboBoxCurar.Location = new System.Drawing.Point(7, 197);
            this.P1_comboBoxCurar.Name = "P1_comboBoxCurar";
            this.P1_comboBoxCurar.Size = new System.Drawing.Size(118, 34);
            this.P1_comboBoxCurar.TabIndex = 9;
            // 
            // P1_listaPD
            // 
            this.P1_listaPD.Enabled = false;
            this.P1_listaPD.FormattingEnabled = true;
            this.P1_listaPD.ItemHeight = 26;
            this.P1_listaPD.Items.AddRange(new object[] {
            "0/0",
            "0/0",
            "0/0",
            "0/0",
            "0/0"});
            this.P1_listaPD.Location = new System.Drawing.Point(445, 57);
            this.P1_listaPD.Name = "P1_listaPD";
            this.P1_listaPD.Size = new System.Drawing.Size(50, 134);
            this.P1_listaPD.TabIndex = 6;
            this.P1_listaPD.UseWaitCursor = true;
            // 
            // P1_listaArmas
            // 
            this.P1_listaArmas.Enabled = false;
            this.P1_listaArmas.FormattingEnabled = true;
            this.P1_listaArmas.ItemHeight = 26;
            this.P1_listaArmas.Items.AddRange(new object[] {
            "Arma 1",
            "Arma 2",
            "Arma 3",
            "Arma 4",
            "Arma 5"});
            this.P1_listaArmas.Location = new System.Drawing.Point(333, 57);
            this.P1_listaArmas.Name = "P1_listaArmas";
            this.P1_listaArmas.Size = new System.Drawing.Size(91, 134);
            this.P1_listaArmas.TabIndex = 6;
            this.P1_listaArmas.UseWaitCursor = true;
            // 
            // P1_botonCurar
            // 
            this.P1_botonCurar.Enabled = false;
            this.P1_botonCurar.Location = new System.Drawing.Point(131, 197);
            this.P1_botonCurar.Name = "P1_botonCurar";
            this.P1_botonCurar.Size = new System.Drawing.Size(95, 34);
            this.P1_botonCurar.TabIndex = 5;
            this.P1_botonCurar.Text = "Curar";
            this.P1_botonCurar.UseVisualStyleBackColor = true;
            this.P1_botonCurar.Click += new System.EventHandler(this.P1_botonCurar_Click);
            // 
            // P1_listaAD
            // 
            this.P1_listaAD.Enabled = false;
            this.P1_listaAD.FormattingEnabled = true;
            this.P1_listaAD.ItemHeight = 26;
            this.P1_listaAD.Items.AddRange(new object[] {
            "20/20 100",
            "20/20 100",
            "20/20 100",
            "20/20 100",
            "20/20 100"});
            this.P1_listaAD.Location = new System.Drawing.Point(200, 57);
            this.P1_listaAD.Name = "P1_listaAD";
            this.P1_listaAD.Size = new System.Drawing.Size(127, 134);
            this.P1_listaAD.TabIndex = 4;
            this.P1_listaAD.UseWaitCursor = true;
            // 
            // P1_botonAtacar
            // 
            this.P1_botonAtacar.Location = new System.Drawing.Point(232, 197);
            this.P1_botonAtacar.Name = "P1_botonAtacar";
            this.P1_botonAtacar.Size = new System.Drawing.Size(95, 34);
            this.P1_botonAtacar.TabIndex = 3;
            this.P1_botonAtacar.Text = "Atacar";
            this.P1_botonAtacar.UseVisualStyleBackColor = true;
            this.P1_botonAtacar.Click += new System.EventHandler(this.P1_botonAtacar_Click);
            // 
            // P1_listaEjercito
            // 
            this.P1_listaEjercito.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.P1_listaEjercito.FormattingEnabled = true;
            this.P1_listaEjercito.ItemHeight = 26;
            this.P1_listaEjercito.Items.AddRange(new object[] {
            "Personaje 1",
            "Personaje 2",
            "Personaje 3",
            "Personaje 4",
            "Personaje 5"});
            this.P1_listaEjercito.Location = new System.Drawing.Point(7, 57);
            this.P1_listaEjercito.Name = "P1_listaEjercito";
            this.P1_listaEjercito.Size = new System.Drawing.Size(186, 134);
            this.P1_listaEjercito.TabIndex = 1;
            this.P1_listaEjercito.SelectedIndexChanged += new System.EventHandler(this.P1_listaEjercito_SelectedIndexChanged);
            // 
            // P2_labelAD
            // 
            this.P2_labelAD.AutoSize = true;
            this.P2_labelAD.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.P2_labelAD.Location = new System.Drawing.Point(200, 32);
            this.P2_labelAD.Name = "P2_labelAD";
            this.P2_labelAD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.P2_labelAD.Size = new System.Drawing.Size(73, 22);
            this.P2_labelAD.TabIndex = 0;
            this.P2_labelAD.Text = "Atq/Def";
            // 
            // P2_labelSalud
            // 
            this.P2_labelSalud.AutoSize = true;
            this.P2_labelSalud.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.P2_labelSalud.Location = new System.Drawing.Point(271, 32);
            this.P2_labelSalud.Name = "P2_labelSalud";
            this.P2_labelSalud.Size = new System.Drawing.Size(56, 22);
            this.P2_labelSalud.TabIndex = 0;
            this.P2_labelSalud.Text = "Salud";
            // 
            // P1_labelPD
            // 
            this.P1_labelPD.AutoSize = true;
            this.P1_labelPD.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.P1_labelPD.Location = new System.Drawing.Point(430, 32);
            this.P1_labelPD.Name = "P1_labelPD";
            this.P1_labelPD.Size = new System.Drawing.Size(81, 22);
            this.P1_labelPD.TabIndex = 0;
            this.P1_labelPD.Text = "Pwr/Dur";
            // 
            // P1_labelArma
            // 
            this.P1_labelArma.AutoSize = true;
            this.P1_labelArma.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.P1_labelArma.Location = new System.Drawing.Point(350, 32);
            this.P1_labelArma.Name = "P1_labelArma";
            this.P1_labelArma.Size = new System.Drawing.Size(56, 22);
            this.P1_labelArma.TabIndex = 0;
            this.P1_labelArma.Text = "Arma";
            // 
            // P1_labelEjercito
            // 
            this.P1_labelEjercito.AutoSize = true;
            this.P1_labelEjercito.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.P1_labelEjercito.Location = new System.Drawing.Point(62, 32);
            this.P1_labelEjercito.Name = "P1_labelEjercito";
            this.P1_labelEjercito.Size = new System.Drawing.Size(76, 22);
            this.P1_labelEjercito.TabIndex = 0;
            this.P1_labelEjercito.Text = "Ejercito";
            // 
            // P1_labelAD
            // 
            this.P1_labelAD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.P1_labelAD.AutoSize = true;
            this.P1_labelAD.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.P1_labelAD.Location = new System.Drawing.Point(241, 32);
            this.P1_labelAD.Name = "P1_labelAD";
            this.P1_labelAD.Size = new System.Drawing.Size(73, 22);
            this.P1_labelAD.TabIndex = 0;
            this.P1_labelAD.Text = "Atq/Def";
            // 
            // P1_labelSalud
            // 
            this.P1_labelSalud.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.P1_labelSalud.AutoSize = true;
            this.P1_labelSalud.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.P1_labelSalud.Location = new System.Drawing.Point(187, 32);
            this.P1_labelSalud.Name = "P1_labelSalud";
            this.P1_labelSalud.Size = new System.Drawing.Size(56, 22);
            this.P1_labelSalud.TabIndex = 0;
            this.P1_labelSalud.Text = "Salud";
            // 
            // P2_groupBox
            // 
            this.P2_groupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.P2_groupBox.BackColor = System.Drawing.Color.Transparent;
            this.P2_groupBox.Controls.Add(this.P2_comboBoxCurar);
            this.P2_groupBox.Controls.Add(this.P2_listaPD);
            this.P2_groupBox.Controls.Add(this.P2_listaArmas);
            this.P2_groupBox.Controls.Add(this.P2_botonCurar);
            this.P2_groupBox.Controls.Add(this.P2_listaAD);
            this.P2_groupBox.Controls.Add(this.P2_botonAtacar);
            this.P2_groupBox.Controls.Add(this.P2_listaEjercito);
            this.P2_groupBox.Controls.Add(this.P2_labelEjercito);
            this.P2_groupBox.Controls.Add(this.P1_labelAD);
            this.P2_groupBox.Controls.Add(this.P2_labelPD);
            this.P2_groupBox.Controls.Add(this.P2_labelArma);
            this.P2_groupBox.Controls.Add(this.P1_labelSalud);
            this.P2_groupBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.P2_groupBox.Location = new System.Drawing.Point(535, 12);
            this.P2_groupBox.Name = "P2_groupBox";
            this.P2_groupBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.P2_groupBox.Size = new System.Drawing.Size(515, 239);
            this.P2_groupBox.TabIndex = 1;
            this.P2_groupBox.TabStop = false;
            this.P2_groupBox.Text = "JUGADOR 2";
            // 
            // P2_comboBoxCurar
            // 
            this.P2_comboBoxCurar.Enabled = false;
            this.P2_comboBoxCurar.FormattingEnabled = true;
            this.P2_comboBoxCurar.Location = new System.Drawing.Point(391, 197);
            this.P2_comboBoxCurar.Name = "P2_comboBoxCurar";
            this.P2_comboBoxCurar.Size = new System.Drawing.Size(118, 34);
            this.P2_comboBoxCurar.TabIndex = 9;
            // 
            // P2_listaPD
            // 
            this.P2_listaPD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.P2_listaPD.Enabled = false;
            this.P2_listaPD.FormattingEnabled = true;
            this.P2_listaPD.ItemHeight = 26;
            this.P2_listaPD.Items.AddRange(new object[] {
            "0/0",
            "0/0",
            "0/0",
            "0/0",
            "0/0"});
            this.P2_listaPD.Location = new System.Drawing.Point(20, 57);
            this.P2_listaPD.Name = "P2_listaPD";
            this.P2_listaPD.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.P2_listaPD.Size = new System.Drawing.Size(50, 134);
            this.P2_listaPD.TabIndex = 6;
            this.P2_listaPD.UseWaitCursor = true;
            // 
            // P2_listaArmas
            // 
            this.P2_listaArmas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.P2_listaArmas.Enabled = false;
            this.P2_listaArmas.FormattingEnabled = true;
            this.P2_listaArmas.ItemHeight = 26;
            this.P2_listaArmas.Items.AddRange(new object[] {
            "Arma 1",
            "Arma 2",
            "Arma 3",
            "Arma 4",
            "Arma 5"});
            this.P2_listaArmas.Location = new System.Drawing.Point(93, 57);
            this.P2_listaArmas.Name = "P2_listaArmas";
            this.P2_listaArmas.Size = new System.Drawing.Size(91, 134);
            this.P2_listaArmas.TabIndex = 6;
            this.P2_listaArmas.UseWaitCursor = true;
            // 
            // P2_botonCurar
            // 
            this.P2_botonCurar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.P2_botonCurar.Enabled = false;
            this.P2_botonCurar.Location = new System.Drawing.Point(290, 197);
            this.P2_botonCurar.Name = "P2_botonCurar";
            this.P2_botonCurar.Size = new System.Drawing.Size(95, 34);
            this.P2_botonCurar.TabIndex = 6;
            this.P2_botonCurar.Text = "Curar";
            this.P2_botonCurar.UseVisualStyleBackColor = true;
            this.P2_botonCurar.Click += new System.EventHandler(this.P2_botonCurar_Click);
            // 
            // P2_listaAD
            // 
            this.P2_listaAD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.P2_listaAD.Enabled = false;
            this.P2_listaAD.FormattingEnabled = true;
            this.P2_listaAD.ItemHeight = 26;
            this.P2_listaAD.Items.AddRange(new object[] {
            "20/20   100",
            "20/20   100",
            "20/20   100",
            "20/20   100",
            "20/20   100"});
            this.P2_listaAD.Location = new System.Drawing.Point(190, 57);
            this.P2_listaAD.Name = "P2_listaAD";
            this.P2_listaAD.Size = new System.Drawing.Size(127, 134);
            this.P2_listaAD.TabIndex = 4;
            this.P2_listaAD.UseWaitCursor = true;
            // 
            // P2_botonAtacar
            // 
            this.P2_botonAtacar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.P2_botonAtacar.Enabled = false;
            this.P2_botonAtacar.Location = new System.Drawing.Point(190, 197);
            this.P2_botonAtacar.Name = "P2_botonAtacar";
            this.P2_botonAtacar.Size = new System.Drawing.Size(95, 34);
            this.P2_botonAtacar.TabIndex = 3;
            this.P2_botonAtacar.Text = "Atacar";
            this.P2_botonAtacar.UseVisualStyleBackColor = true;
            this.P2_botonAtacar.Click += new System.EventHandler(this.P2_botonAtacar_Click);
            // 
            // P2_listaEjercito
            // 
            this.P2_listaEjercito.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.P2_listaEjercito.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.P2_listaEjercito.FormattingEnabled = true;
            this.P2_listaEjercito.ItemHeight = 26;
            this.P2_listaEjercito.Items.AddRange(new object[] {
            "Personaje 1",
            "Personaje 2",
            "Personaje 3",
            "Personaje 4",
            "Personaje 5"});
            this.P2_listaEjercito.Location = new System.Drawing.Point(323, 57);
            this.P2_listaEjercito.Name = "P2_listaEjercito";
            this.P2_listaEjercito.Size = new System.Drawing.Size(186, 134);
            this.P2_listaEjercito.TabIndex = 1;
            this.P2_listaEjercito.SelectedIndexChanged += new System.EventHandler(this.P2_listaEjercito_SelectedIndexChanged);
            // 
            // P2_labelEjercito
            // 
            this.P2_labelEjercito.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.P2_labelEjercito.AutoSize = true;
            this.P2_labelEjercito.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.P2_labelEjercito.Location = new System.Drawing.Point(374, 32);
            this.P2_labelEjercito.Name = "P2_labelEjercito";
            this.P2_labelEjercito.Size = new System.Drawing.Size(76, 22);
            this.P2_labelEjercito.TabIndex = 0;
            this.P2_labelEjercito.Text = "Ejercito";
            // 
            // P2_labelPD
            // 
            this.P2_labelPD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.P2_labelPD.AutoSize = true;
            this.P2_labelPD.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.P2_labelPD.Location = new System.Drawing.Point(6, 32);
            this.P2_labelPD.Name = "P2_labelPD";
            this.P2_labelPD.Size = new System.Drawing.Size(81, 22);
            this.P2_labelPD.TabIndex = 0;
            this.P2_labelPD.Text = "Pwr/Dur";
            // 
            // P2_labelArma
            // 
            this.P2_labelArma.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.P2_labelArma.AutoSize = true;
            this.P2_labelArma.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.P2_labelArma.Location = new System.Drawing.Point(111, 32);
            this.P2_labelArma.Name = "P2_labelArma";
            this.P2_labelArma.Size = new System.Drawing.Size(56, 22);
            this.P2_labelArma.TabIndex = 0;
            this.P2_labelArma.Text = "Arma";
            // 
            // boton_AgarrarArma
            // 
            this.boton_AgarrarArma.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.boton_AgarrarArma.Location = new System.Drawing.Point(446, 257);
            this.boton_AgarrarArma.Name = "boton_AgarrarArma";
            this.boton_AgarrarArma.Size = new System.Drawing.Size(172, 34);
            this.boton_AgarrarArma.TabIndex = 6;
            this.boton_AgarrarArma.Text = "Agarrar Arma";
            this.boton_AgarrarArma.UseVisualStyleBackColor = true;
            this.boton_AgarrarArma.Click += new System.EventHandler(this.boton_AgarrarArma_Click);
            // 
            // imagenCofre
            // 
            this.imagenCofre.BackColor = System.Drawing.Color.Transparent;
            this.imagenCofre.Image = ((System.Drawing.Image)(resources.GetObject("imagenCofre.Image")));
            this.imagenCofre.Location = new System.Drawing.Point(425, 297);
            this.imagenCofre.Name = "imagenCofre";
            this.imagenCofre.Size = new System.Drawing.Size(232, 179);
            this.imagenCofre.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imagenCofre.TabIndex = 7;
            this.imagenCofre.TabStop = false;
            // 
            // textBoxCajon
            // 
            this.textBoxCajon.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.textBoxCajon.Location = new System.Drawing.Point(471, 345);
            this.textBoxCajon.Name = "textBoxCajon";
            this.textBoxCajon.ReadOnly = true;
            this.textBoxCajon.Size = new System.Drawing.Size(126, 26);
            this.textBoxCajon.TabIndex = 8;
            this.textBoxCajon.Text = "Arma - 15 / 5";
            this.textBoxCajon.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::JuegoDeRol.UI.Properties.Resources.ArenaBackground;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1062, 483);
            this.Controls.Add(this.textBoxCajon);
            this.Controls.Add(this.imagenCofre);
            this.Controls.Add(this.boton_AgarrarArma);
            this.Controls.Add(this.P2_groupBox);
            this.Controls.Add(this.P1_groupBox);
            this.Name = "Form1";
            this.Text = "Clash of Armies";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.P1_groupBox.ResumeLayout(false);
            this.P1_groupBox.PerformLayout();
            this.P2_groupBox.ResumeLayout(false);
            this.P2_groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imagenCofre)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox P1_groupBox;
        private System.Windows.Forms.ListBox P1_listaEjercito;
        private System.Windows.Forms.Label P1_labelSalud;
        private System.Windows.Forms.Label P1_labelEjercito;
        private System.Windows.Forms.Label P1_labelAD;
        private System.Windows.Forms.Button P1_botonAtacar;
        private System.Windows.Forms.GroupBox P2_groupBox;
        private System.Windows.Forms.Button P2_botonAtacar;
        private System.Windows.Forms.ListBox P2_listaEjercito;
        private System.Windows.Forms.Label P2_labelAD;
        private System.Windows.Forms.Label P2_labelSalud;
        private System.Windows.Forms.Label P2_labelEjercito;
        private System.Windows.Forms.ListBox P1_listaAD;
        private System.Windows.Forms.ListBox P2_listaAD;
        private System.Windows.Forms.ListBox P1_listaArmas;
        private System.Windows.Forms.Button P1_botonCurar;
        private System.Windows.Forms.Button P2_botonCurar;
        private System.Windows.Forms.Button boton_AgarrarArma;
        private System.Windows.Forms.ListBox P2_listaArmas;
        private System.Windows.Forms.PictureBox imagenCofre;
        private System.Windows.Forms.TextBox textBoxCajon;
        private System.Windows.Forms.ListBox P1_listaPD;
        private System.Windows.Forms.ListBox P2_listaPD;
        private System.Windows.Forms.Label P1_labelPD;
        private System.Windows.Forms.Label P1_labelArma;
        private System.Windows.Forms.Label P2_labelPD;
        private System.Windows.Forms.Label P2_labelArma;
        private System.Windows.Forms.ComboBox P1_comboBoxCurar;
        private System.Windows.Forms.ComboBox P2_comboBoxCurar;
    }
}

