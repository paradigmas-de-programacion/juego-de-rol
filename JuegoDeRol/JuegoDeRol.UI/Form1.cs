﻿using System;
using System.Windows.Forms;
using JuegoDeRol.Negocio;
using JuegoDeRol.Negocio.Armas;
using Microsoft.VisualBasic;

namespace JuegoDeRol.UI
{
    public enum TiposDeArmas
    {
        Arco, Ballesta, Daga, Espada, Hechizo, Mazo
    }
    public partial class Form1 : Form
    {
        Ejercito P1_Ejercito;
        Ejercito P2_Ejercito;
        Personaje P1_PersonajeSeleccionado;
        Personaje P2_PersonajeSeleccionado;
        int[] puntajes = { 0, 0 };
        Arma armaCajon;

        public Form1() { InitializeComponent(); }

        private void NuevaPartida()
        {
            P1_Ejercito = new(Interaction.InputBox("Ingrese el nombre del Jugador 1: ", "Juego De Rol", "Jugador 1"));
                P1_groupBox.Text = P1_Ejercito.NombreEjercito;
            P2_Ejercito = new(Interaction.InputBox("Ingrese el nombre del Jugador 2: ", "Juego De Rol", "Jugador 2"));
                P2_groupBox.Text = P2_Ejercito.NombreEjercito;
        }
        private void NuevaRonda()
        {
            Text = $"Juego de Rol: {puntajes[0]} - {puntajes[1]}";
            P1_Ejercito = new(P1_Ejercito.NombreEjercito);
            P2_Ejercito = new(P2_Ejercito.NombreEjercito);
        }
        private void RefrescarListbox(ListBox listaNombre, ListBox listaAd, ListBox listaArmas, ListBox listaStats, ComboBox comboBoxCuraciones, Ejercito ejercito)
        {
            listaNombre.Items.Clear(); listaAd.Items.Clear(); listaArmas.Items.Clear(); listaStats.Items.Clear(); comboBoxCuraciones.Items.Clear();
            foreach (Personaje p in ejercito.Tropas)
            {
                listaNombre.Items.Add(p);
                listaAd.Items.Add((string)$"{p.FuerzaAtaque}/{p.Defensa}   {p.Salud}");
                listaArmas.Items.Add(p.ArmaActual.GetType().Name);
                listaStats.Items.Add($"{p.ArmaActual.MostrarStats()}");
                comboBoxCuraciones.Items.Add(p);
            }
            //Actualizar cajon de armas
            ActualizarCajon();
        }
        private void ActualizarCajon()
        {
            Random r = new(System.DateTime.Now.Millisecond);
            int i = r.Next(0, Enum.GetNames(typeof(TiposDeArmas)).Length);
            switch (i)
            {
                case 0:
                    armaCajon = new Arco();
                    break;
                case 1:
                    armaCajon = new Ballesta();
                    break;
                case 2:
                    armaCajon = new Daga();
                    break;
                case 3:
                    armaCajon = new Espada();
                    break;
                case 4:
                    armaCajon = new Hechizo();
                    break;
                case 5:
                    armaCajon = new Mazo();
                    break;
            }
            textBoxCajon.Text = armaCajon.MostrarInfo();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            NuevaPartida();
            NuevaRonda();
            RefrescarListbox(P1_listaEjercito, P1_listaAD, P1_listaArmas, P1_listaPD, P1_comboBoxCurar, P1_Ejercito);
            RefrescarListbox(P2_listaEjercito, P2_listaAD, P2_listaArmas, P2_listaPD, P2_comboBoxCurar, P2_Ejercito);
        }

        private void P1_listaEjercito_SelectedIndexChanged(object sender, EventArgs e)
        {
            P1_PersonajeSeleccionado = (Personaje)P1_listaEjercito.SelectedItem;
            if (P1_PersonajeSeleccionado == null)
            {
                P1_listaAD.ClearSelected();
                P1_listaArmas.ClearSelected();
            } else
            {
                P1_listaAD.SelectedIndex = P1_listaEjercito.SelectedIndex;
                P1_listaArmas.SelectedIndex = P1_listaAD.SelectedIndex;
            }
            bool puedeCurar = (P1_botonAtacar.Enabled == true && P1_PersonajeSeleccionado is Mago && P1_PersonajeSeleccionado.ArmaActual is Hechizo) ? true : false;
            P1_botonCurar.Enabled = puedeCurar; P1_comboBoxCurar.Enabled = puedeCurar;
        }
        private void P2_listaEjercito_SelectedIndexChanged(object sender, EventArgs e)
        {
            P2_PersonajeSeleccionado = (Personaje)P2_listaEjercito.SelectedItem;
            if (P2_PersonajeSeleccionado == null)
            {
                P2_listaAD.ClearSelected();
                P2_listaArmas.ClearSelected();
            }
            else
            {
                P2_listaAD.SelectedIndex = P2_listaEjercito.SelectedIndex;
                P2_listaArmas.SelectedIndex = P2_listaAD.SelectedIndex;
            }
            bool puedeCurar = (P2_botonAtacar.Enabled == true && P2_PersonajeSeleccionado is Mago && P2_PersonajeSeleccionado.ArmaActual is Hechizo) ? true : false;
            P2_botonCurar.Enabled = puedeCurar; P2_comboBoxCurar.Enabled = puedeCurar;
        }

        private void P1_botonAtacar_Click(object sender, EventArgs e)
        {
            try
            {
                //Validacion seleccion ataque y defensa
                if (P1_listaEjercito.SelectedItem == null) { throw new Exception("Por favor seleccione un personaje para atacar!"); }
                if (P2_listaEjercito.SelectedItem == null) { throw new Exception("Por favor seleccione un objetivo para atacar!"); }
                //Atacar
                MessageBox.Show(P1_PersonajeSeleccionado.Atacar(P2_PersonajeSeleccionado));
                //Comprobar si el enemigo atacado murió
                if (P2_PersonajeSeleccionado.Salud <= 0)
                {
                    MessageBox.Show($"{P2_PersonajeSeleccionado.Nombre} ha caído en batalla!");
                    P2_Ejercito.Tropas.Remove(P2_PersonajeSeleccionado);
                    P2_PersonajeSeleccionado = null;
                }
                //Comprobar si el ejercito enemigo está vencido
                if (P2_Ejercito.Tropas.Count <= 0)
                {
                    MessageBox.Show($"El ejercito de {P2_Ejercito.NombreEjercito} ha sido derrotado!\n{P1_Ejercito.NombreEjercito} gana la ronda {puntajes[0]+puntajes[1]}");
                    puntajes[0]++;
                    NuevaRonda();
                }
                //Refrescar listas
                RefrescarListbox(P1_listaEjercito, P1_listaAD, P1_listaArmas, P1_listaPD, P1_comboBoxCurar, P1_Ejercito);
                RefrescarListbox(P2_listaEjercito, P2_listaAD, P2_listaArmas, P2_listaPD, P2_comboBoxCurar, P2_Ejercito);
                //Desabilitar Input P1
                P1_botonAtacar.Enabled = false; P2_botonAtacar.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void P2_botonAtacar_Click(object sender, EventArgs e)
        {
            try
            {
                //Validacion seleccion ataque y defensa
                if (P2_listaEjercito.SelectedItem == null) { throw new Exception("Por favor seleccione un personaje para atacar!"); }
                if (P1_listaEjercito.SelectedItem == null) { throw new Exception("Por favor seleccione un objetivo para atacar!"); }
                //Atacar
                MessageBox.Show(P2_PersonajeSeleccionado.Atacar(P1_PersonajeSeleccionado));
                //Comprobar si el enemigo atacado murió
                if (P1_PersonajeSeleccionado.Salud <= 0)
                {
                    MessageBox.Show($"{P1_PersonajeSeleccionado.Nombre} ha caído en batalla!");
                    P1_Ejercito.Tropas.Remove(P1_PersonajeSeleccionado);
                    P1_PersonajeSeleccionado = null;
                }
                //Comprobar si el ejercito enemigo está vencido
                if (P1_Ejercito.Tropas.Count <= 0)
                {
                    MessageBox.Show($"El ejercito de {P1_Ejercito.NombreEjercito} ha sido derrotado!\n{P2_Ejercito.NombreEjercito} gana la ronda {puntajes[0] + puntajes[1]}");
                    puntajes[1]++;
                    NuevaRonda();
                }
                //Refrescar listas
                RefrescarListbox(P1_listaEjercito, P1_listaAD, P1_listaArmas, P1_listaPD, P1_comboBoxCurar, P1_Ejercito);
                RefrescarListbox(P2_listaEjercito, P2_listaAD, P2_listaArmas, P2_listaPD, P2_comboBoxCurar, P2_Ejercito);
                //Desabilitar Input P1
                P1_botonAtacar.Enabled = true; P2_botonAtacar.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void boton_AgarrarArma_Click(object sender, EventArgs e)
        {
            try
            {
                if (P1_botonAtacar.Enabled)
                {
                    //Validacion seleccion personaje
                    if (P1_listaEjercito.SelectedItem == null) { throw new Exception("Por favor seleccione un personaje con el cual tomar el arma!"); }
                    //Tomar Arma del Cajón
                    P1_PersonajeSeleccionado.AgararArma(armaCajon);
                    //Refrescar lista
                    RefrescarListbox(P1_listaEjercito, P1_listaAD, P1_listaArmas, P1_listaPD, P1_comboBoxCurar, P1_Ejercito);
                    //Deshabilitar Input P1
                    P1_botonAtacar.Enabled = false; P2_botonAtacar.Enabled = true;
                }
                else if (P2_botonAtacar.Enabled)
                {
                    //Validacion seleccion personaje
                    if (P2_listaEjercito.SelectedItem == null) { throw new Exception("Por favor seleccione un personaje con el cual tomar el arma!"); }
                    //Tomar Arma del Cajón
                    P2_PersonajeSeleccionado.AgararArma(armaCajon);
                    //Refrescar lista
                    RefrescarListbox(P2_listaEjercito, P2_listaAD, P2_listaArmas, P2_listaPD, P2_comboBoxCurar, P2_Ejercito);
                    //Deshabilitar Input P1
                    P1_botonAtacar.Enabled = true; P2_botonAtacar.Enabled = false;
                } else
                {
                    throw new Exception("Este error nunca debería triggerearse!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void P1_botonCurar_Click(object sender, EventArgs e)
        {
            try
            {
                if (P1_PersonajeSeleccionado.ArmaActual is not Hechizo)
                {
                    throw new Exception($"{P1_PersonajeSeleccionado.Nombre} debe tener un hechizo como arma principal para poder curar!");
                }
                if (P1_comboBoxCurar.SelectedItem == null)
                {
                    throw new Exception($"Seleccione un personaje del dropdown a quien {P1_PersonajeSeleccionado.Nombre} va a curar!");
                }
                if (P1_comboBoxCurar.SelectedItem == P1_listaEjercito.SelectedItem)
                {
                    throw new Exception($"{P1_PersonajeSeleccionado.Nombre} no puede curarse a si mismo!");
                }
                ((Mago)P1_PersonajeSeleccionado).Curar((Personaje)P1_comboBoxCurar.SelectedItem);
                //Refrescar listas
                RefrescarListbox(P1_listaEjercito, P1_listaAD, P1_listaArmas, P1_listaPD, P1_comboBoxCurar, P1_Ejercito);
                RefrescarListbox(P2_listaEjercito, P2_listaAD, P2_listaArmas, P2_listaPD, P2_comboBoxCurar, P2_Ejercito);
                //Desabilitar Input P1
                P1_botonCurar.Enabled = false; P1_comboBoxCurar.Enabled = false;
                P1_botonAtacar.Enabled = false; P2_botonAtacar.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void P2_botonCurar_Click(object sender, EventArgs e)
        {
            try
            {
                if (P2_PersonajeSeleccionado.ArmaActual is not Hechizo)
                {
                    throw new Exception($"{P2_PersonajeSeleccionado.Nombre} debe tener un hechizo como arma principal para poder curar!");
                }
                if (P2_comboBoxCurar.SelectedItem == null)
                {
                    throw new Exception($"Seleccione un personaje del dropdown a quien {P2_PersonajeSeleccionado.Nombre} va a curar!");
                }
                if (P2_comboBoxCurar.SelectedItem == P2_listaEjercito.SelectedItem)
                {
                    throw new Exception($"{P2_PersonajeSeleccionado.Nombre} no puede curarse a si mismo!");
                }
                ((Mago)P2_PersonajeSeleccionado).Curar((Personaje)P2_comboBoxCurar.SelectedItem);
                //Refrescar listas
                RefrescarListbox(P1_listaEjercito, P1_listaAD, P1_listaArmas, P1_listaPD, P1_comboBoxCurar, P1_Ejercito);
                RefrescarListbox(P2_listaEjercito, P2_listaAD, P2_listaArmas, P2_listaPD, P2_comboBoxCurar, P2_Ejercito);
                //Desabilitar Input P1
                P2_botonCurar.Enabled = false;  P2_comboBoxCurar.Enabled = false;
                P2_botonAtacar.Enabled = false; P1_botonAtacar.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}