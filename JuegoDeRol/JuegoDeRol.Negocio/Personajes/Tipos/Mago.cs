﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JuegoDeRol.Negocio.Armas;
using JuegoDeRol.Negocio.Interfaces;

namespace JuegoDeRol.Negocio
{

    public class Mago : Personaje
    {
        protected override string[] Nombres { get; } = { "Gandalf", "Saruman", "Radagast", "Alatar", "Pallando" };

        public string Curar(Personaje receptor)
        {
            int cantidad = ArmaActual.Potencia * 2;
            receptor.RecibirCuracion(cantidad);
            ChequearDurabilidadArma();
            return (string)$"{Nombre} curó a {receptor.Nombre} por {cantidad} puntos de salud!";
        }
        public override void AgararArma(Arma arma)
        {
            base.AgararArma(arma);
            if (arma is MagoUtilizable)
            {
                this.ArmaActual = arma;
            }
            else
            {
                throw new Exception($"El arma: {arma.GetType().Name} no puede ser utilizada por un {GetType().Name}");
            }
        }

        //Constructores
        public Mago(int defensa, int fuerzaAtaque) : base(defensa, fuerzaAtaque)
        {
            this.Nombre = "🔮";
            ElegirNombre();
        }
    }
}
