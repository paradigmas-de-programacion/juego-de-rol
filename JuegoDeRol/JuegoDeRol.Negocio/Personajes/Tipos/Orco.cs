﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JuegoDeRol.Negocio.Armas;
using JuegoDeRol.Negocio.Interfaces;

namespace JuegoDeRol.Negocio
{
    public class Orco : Personaje
    {
        protected override string[] Nombres { get; } = { "Uglúk", "Ogbokh", "Urbac", "Mauhúr", "Lugdush" };
        public override int SaludMaxima { get; protected set; } = 100;
        public override void AgararArma(Arma arma)
        {
            base.AgararArma(arma);
            if (arma is OrcoUtilizable)
            {
                this.ArmaActual = arma;
            }
            else
            {
                throw new Exception($"El arma: {arma.GetType().Name} no puede ser utilizada por un {GetType().Name}");
            }
        }

        //Constructores
        public Orco(int defensa, int fuerzaAtaque) : base(defensa, fuerzaAtaque)
        {
            this.Nombre = "🛡";
            ElegirNombre();
        }
    }
}
