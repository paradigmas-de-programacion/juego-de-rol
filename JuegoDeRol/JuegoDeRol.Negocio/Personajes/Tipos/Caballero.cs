﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JuegoDeRol.Negocio.Armas;
using JuegoDeRol.Negocio.Interfaces;

namespace JuegoDeRol.Negocio
{
    public class Caballero : Personaje
    {
        protected override string[] Nombres { get; } = { "Aragorn", "Isildur", "Boromir", "Faramir", "Beregond" };
        public override void AgararArma(Arma arma)
        {
            base.AgararArma(arma);
            if (arma is CaballeroUtilizable)
            {
                this.ArmaActual = arma;
            }
            else
            {
                throw new Exception($"El arma: {arma.GetType().Name} no puede ser utilizada por un {GetType().Name}");
            }
        }
        //Constructores
        public Caballero(int defensa, int fuerzaAtaque) : base(defensa, fuerzaAtaque)
        {
            this.Nombre = "⚔";
            ElegirNombre();
        }
    }
}
