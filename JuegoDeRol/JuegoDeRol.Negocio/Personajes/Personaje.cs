﻿using System;
using System.Diagnostics;
using JuegoDeRol.Negocio.Armas;

namespace JuegoDeRol.Negocio
{
    public class Personaje
    {
        ///Propiedades
        public string Nombre { get; protected set; }
        public int Salud { get; protected set; }
        public virtual int SaludMaxima { get; protected set; } = 50;
        public int FuerzaAtaque { get; private set; }
        public int Defensa { get; private set; }
        public virtual Arma ArmaActual { get; protected set; }
        protected virtual string[] Nombres { get; }

        /// Metodos
        //Metodo que sirve para asignar un nombre
        public void ElegirNombre()
        {
            Random r = new();
            System.Threading.Thread.Sleep(10);
            string nombre = Nombres[r.Next(0, Nombres.Length)];
            this.Nombre += $" {nombre}";
        }
        //Metodo que sirve para atacar
        public virtual string Atacar(Personaje receptor)
        {
            Random r = new Random(); //Instancio un objeto de clase Random
            int cantidad = r.Next(0, 10) + FuerzaAtaque; //Devuelvo el daño de mi ataque: un valor entre 0 y 10 + la fuerza de ataque
            if (cantidad > receptor.Defensa)
            {
                if (ArmaActual is not Puño)
                {
                    cantidad += ArmaActual.Potencia;
                    ChequearDurabilidadArma();
                }
                receptor.Salud -= (cantidad-FuerzaAtaque);
                return $"{this.Nombre} atacó a {receptor.Nombre} por {cantidad-FuerzaAtaque} puntos de salud!";
            }
            else
            {
                return $"El ataque de {this.Nombre} hacia {receptor.Nombre} falló!";
            }
        }
        //Metodo que sirve para agarrar un arma
        public virtual void AgararArma(Arma arma)
        {
            if (arma is Puño)
            {
                ArmaActual = arma;
            }
        }
        //Metodo para ver si reducir la durabilidad de un arma y/o eliminar el arma actual
        protected void ChequearDurabilidadArma()
        {
            if (ArmaActual.GetType().Name != "Puño")
            {
                ArmaActual.ReducirDurabilidad(1);
            }
            if (ArmaActual.Durabilidad <= 0)
            {
                ArmaActual = new Puño();
            }
        }
        //Metodo para recibir curaciones
        public virtual void RecibirCuracion(int cantidad)
        {
            if (Salud != SaludMaxima)
            {
                if (Salud + cantidad <= SaludMaxima)
                {
                    this.Salud += cantidad;
                }
                else
                {
                    this.Salud = SaludMaxima;
                }
            }
        }
        
        ///Constructores
        public Personaje(int defensa, int fuerzaAtaque)
        {
            this.Salud = SaludMaxima;
            this.Defensa = defensa;
            this.FuerzaAtaque = fuerzaAtaque;
            this.ArmaActual = new Puño();
        }

        public override string ToString() { return this.Nombre; }
    }
}
