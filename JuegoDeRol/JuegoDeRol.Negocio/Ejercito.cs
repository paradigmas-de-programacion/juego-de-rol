﻿using System;
using System.Collections.Generic;

namespace JuegoDeRol.Negocio
{
    public class Ejercito
    {
        public string NombreEjercito { get; private set; }
        private int TamañoEjercito { get; } = 5;
        public List<Personaje> Tropas { get; private set; } = new();

        public Ejercito(string nombre)
        {
            NombreEjercito = nombre;
            
            for (int i = 0; i < TamañoEjercito; i++)
            {
                System.Threading.Thread.Sleep(10);
                Random r = new(System.DateTime.Now.Millisecond);
                int clase = r.Next(0, 4);
                switch (clase)
                {
                    case 0:
                        Tropas.Add(new Caballero(25, 15)); //+Defensa  -Daño
                        break;
                    case 1:
                        Tropas.Add(new Arquero(20, 20));   //~Defensa  ~Daño
                        break;
                    case 2:
                        Tropas.Add(new Mago(15, 25));  //-Defensa  +Daño
                        break;
                    case 3:
                        Tropas.Add(new Orco(20, 20));      //+Vida ~Defensa ~Daño
                        break;
                }
            }
        }
    }
}
