﻿using System;
using JuegoDeRol.Negocio.Interfaces;

namespace JuegoDeRol.Negocio.Armas
{
    public class Arma
    {
        //Propiedades
        public int Potencia { get; protected set; }
        public int Durabilidad { get; protected set; }

        public void ReducirDurabilidad(int cantidad)
        {
            Durabilidad -= cantidad;
        }

        public string MostrarInfo()
        {
            return $"{(string)GetType().Name} - {this.Potencia}/{this.Durabilidad}";
        }
        public virtual string MostrarStats()
        {
            return $"{this.Potencia}/{this.Durabilidad}";
        }
    }
}
