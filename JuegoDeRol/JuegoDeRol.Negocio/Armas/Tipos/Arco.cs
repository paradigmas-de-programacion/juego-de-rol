﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JuegoDeRol.Negocio.Interfaces;

namespace JuegoDeRol.Negocio.Armas
{
    public class Arco : Arma , ArqueroUtilizable
    {
        public Arco()
        {
            Potencia = 10;
            Durabilidad = 5;
        }
    }
}
