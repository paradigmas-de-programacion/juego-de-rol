﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JuegoDeRol.Negocio.Interfaces;

namespace JuegoDeRol.Negocio.Armas
{
    public class Ballesta : Arma , ArqueroUtilizable
    {
        public Ballesta()
        {
            Potencia = 5;
            Durabilidad = 4;
        }
    }
}
