﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JuegoDeRol.Negocio.Interfaces;

namespace JuegoDeRol.Negocio.Armas
{
    public class Hechizo : Arma, MagoUtilizable
    {
        public Hechizo()
        {
            Potencia = 10;
            Durabilidad = 5;
        }
    }
}
