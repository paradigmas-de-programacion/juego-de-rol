﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JuegoDeRol.Negocio.Interfaces;

namespace JuegoDeRol.Negocio.Armas
{
    public class Puño : Arma
    {
        public Puño()
        {
            Potencia = 1;
            Durabilidad = -2;
        }
        public override string MostrarStats()
        {
            return $"{this.Potencia}/-";
        }
    }
}
