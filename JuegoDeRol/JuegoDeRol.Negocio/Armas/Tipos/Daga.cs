﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JuegoDeRol.Negocio.Interfaces;

namespace JuegoDeRol.Negocio.Armas
{
    public class Daga : Arma , CaballeroUtilizable , MagoUtilizable , ArqueroUtilizable
    {
        public Daga()
        {
            Potencia = 3;
            Durabilidad = 3;
        }
    }
}
